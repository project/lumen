<div id="<?php print $node_id ?>" class="node <?php print $classes ?>">
  <div class="container <?php print $tax_classes ?>">

    <?php if (!$page) : ?>
    <h2 class="title">
      <a href="<?php print $node_url ?>"><?php print $title ?></a>
    </h2>
    <?php endif; ?>

    <div class="meta">
      <?php if ($submitted) : ?>
      <?php print $picture // div.picture img ?>
      <span class="submitted" title="<?php print $r_created ?>"><?php print $name .' '. $date ?></span>
      <?php endif; ?>
      <?php if ($terms) : ?>
      <?php print $terms // ul.links.term-links ?>
      <?php endif; ?>
    </div>

    <?php print $node_region // Block region -> block.tpl.php ?>
    
    <?php print $content // Node content. ?>
    
    <?php print $links // ul.links.node-links ?>
    
  </div>
  <hr class="clear hidden" />
</div>
