<div id="<?php print $block_id ?>" class="block <?php print $classes ?>">
  
  <?php if ($block->subject) : ?>
  <h3 class="title">
    <?php print $block->subject; ?>
  </h3>
  <?php endif; ?>
  
  <?php print $block->content; ?>
  
  <hr class="hidden clear" />
</div>
