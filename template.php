<?php

/* #################### PHPTEMPLATE OVERRIDES #################### */

/**
 * Intercept and pass variables to templates. Part of _phptemplate_callback().
 *
 * @param $hook
 *  Theme function being run, e.g. 'node', 'page', etc...
 * @param $vars
 *  Array of variables set before this function.
 * @return
 *  Variables to be extracted into .tpl.php files.
 */
function _phptemplate_variables($hook, $vars = array()) {
  // $user is the current user. Needed for manipulations set below.
  global $user;

  switch ($hook) {

    /* === NODE VARIABLES === */
    case 'node':
    { 
      // $node_classes passed through static for $hook == 'page'.
      if (!$vars['teaser']) {
        static $node_classes = array();
      }
      
      $vars['node_id'] = 'node-unsaved';
      if (isset($vars['node']->nid)) {
        $vars['node_id'] = 'node-'. $vars['node']->nid;
        // Strip spaces and prepare for lumen_make_classes(). Cut up so there's only a single time measure.
        $r_created = substr($vars['r_created'], 0, strpos($vars['r_created'], ' ', 5));
        $r_created = $r_created ? str_replace(' ', '-', $r_created) : 'lt-min';
        // $vars['r_changed'] available when creation and modification date is beyond a minute apart.
        if ($vars['r_changed']) {
          $r_changed = substr($vars['r_changed'], 0, strpos($vars['r_changed'], ' ', 5));
          if ($r_changed != $r_created) { // If not equal within the same time measure.
            $r_changed = $r_changed ? str_replace(' ', '-', $r_changed) : 'lt-min';
          }
        }
      }
      // Process classes. Look at lumen_make_classes() notes for expected input.
      $node_classes = lumen_make_classes(array(
        'is-front'          =>      $vars['is_front'],
        'teaser'            =>      $vars['teaser'],
        'read-more'         =>      $vars['teaser'] && $vars['node']->readmore,
        $vars['node']->type =>      TRUE,
        'by-viewer'         =>      $vars['node']->uid == $user->uid && $user->uid != 0,
        'by-anonymous'      =>      $vars['node']->uid == 0,
        'promoted'          =>      $vars['node']->promote,
        'sticky'            =>      $vars['node']->sticky,
        'unpublished'       =>    ! $vars['node']->status,
        'cre'               =>      isset($r_created) ? '-'. $r_created : '',
        'chg'               =>      isset($r_changed) ? '-'. $r_changed : '',
        $vars['zebra']      =>      $vars['teaser'],
        'pos'               =>      $vars['teaser'] ? '-'. $vars['id'] : '',
      ), $hook, FALSE);
      // Flatten $classes and prepend $hook. "node-" in this case.
      $vars['classes'] = "$hook-". implode(" $hook-", $node_classes);
      // Set taxonomy classes. Output from lumen_alt_taxonomy().
      $vars['tax_classes'] = !empty($vars['tax_list']) ? "$hook-". implode(" $hook-", $vars['tax_list']) : 'un-tagged';
      
      break;
    }

    /* === COMMENT VARIABLES === */
    case 'comment':
    { 
      $vars['comment_id'] = 'comment-'. (isset($vars['comment']->cid) ? $vars['comment']->cid : 'unsaved');
      // Class for relative age from time stamp.
      $r_created = isset($vars['comment']->cid) ? str_replace(' ', '-', format_interval(time() - $vars['comment']->timestamp, 1)) : '';
      // For read status class. Mapped: 0 = read, 1 = new, 2 = updated.
      $mark = array('read', 'new', 'updated');
      $vars['classes'] = lumen_make_classes(array(
        'is-front'                    =>      $vars['is_front'],
        'by-viewer'                   =>      $vars['comment']->uid == $user->uid && $user->uid != 0,
        'by-node-author'              =>      $vars['comment']->uid == $vars['node_uid'] && $user->uid != 0,
        'by-anonymous'                =>      $vars['comment']->uid == 0,
        'unpublished'                 =>      $vars['comment']->status,
        'depth'                       =>    ! arg(2) ? '-'. $vars['comment']->depth : '',
        $mark[$vars['comment']->new]  =>      $user->uid != 0,
        'cre'                         => '-'. $r_created,
        $vars['zebra']                =>    ! arg(2),
        'pos'                         =>    ! arg(2) ? '-'. $vars['id'] : '',
      ), $hook);
      
      break;
    }

    /* === BLOCK VARIABLES === */
    case 'block':
    { 
      $vars['block_id'] = 'block-'. $vars['module'] .'-'. $vars['block']->delta;
      $vars['classes'] = lumen_make_classes(array(
        'is-front'      =>      $vars['is_front'],
        $vars['module'] =>      TRUE,
        $vars['region'] =>      TRUE,
        'throttled'     =>      $vars['block']->throttle,
        $vars['zebra']  =>      TRUE,
        'pos'           => '-'. $vars['id'],
      ), $hook);
      
      break;
    }

    /* === BOX VARIABLES === */
  //case 'box':
    {
      // Just a place holder.

    //break;
    }

    /* === PAGE VARIABLES === */
    case 'page':
    { 
      $pos_map = array(-1 => 'last', 1 => 'first', 0 => 'single');
      $page_classes = array_merge(lumen_make_classes(array(
        'is-front'  =>      $vars['is_front'],
        'layout'    => '-'. $vars['layout'],
        'lang'      => '-'. $vars['language'],
      ), $hook, FALSE), $vars['alt_data'], isset($node_classes) ? $node_classes : array());
      $vars['classes'] = "$hook-". implode(" $hook-", $page_classes);
      
      break;
    }

  }
  
  return $vars;
}

/**
 * Declare the available regions implemented by this engine.
 *
 * Listed order below reflects the menu order in block administration page.
 *
 * $other_regions are added programatically and added to $block_filter_list.
 * This function works closely with phptemplate_blocks(). $block_filter_list
 * prevents the listed regions from being rendered when theme_page is run.
 * See notes on phptemplate_blocks() for more information.
 *
 * @return
 *  An array of regions. The first array element will be used as the default
 *  region for themes.
 */
function lumen_regions() {
  global $block_filter_list; // Used for phptemplate_blocks.

  $page_regions = array(
    'left'          => t('sidebar left'),   // page.tpl.php -> $sidebar_left
    'right'         => t('sidebar right'),  // page.tpl.php -> $sidebar_right
    'content'       => t('main content'),   // page.tpl.php -> $content
    'header_region' => t('site header'),    // page.tpl.php -> $header_region
    'footer_region' => t('site footer'),    // page.tpl.php -> $footer_region
  );
  // Set for lumen_node() (node specific regions).
  // node.tpl.php -> $node_region
  $other_regions['node_region'] = t('node -all types');
  foreach (node_get_types('names') as $type => $name) {
    // node.tpl.php -> $node_type_region
    $other_regions['node_region_'. $type] = t('node !type', array('!type' => $name));
  }
  // Passed through phptemplate_comment_wrapper() -in this file. It is *not* added to each comment.
  // If comments are disabled or the viewer does not have permmission to view, then the region will not show.
  // templates_overrides/comment_wrapper.tpl.php -> $comment_region
  $other_regions['comment_region'] = t('comment -all types');
  foreach (node_get_types('names') as $type => $name) {
    // templates_overrides/comment_wrapper.tpl.php -> $comment_type_region
    $other_regions['comment_region_'. $type] = t('comment !type', array('!type' => $name));
  }
  // Set filter list.
  $block_filter_list = $other_regions;
  
  return $page_regions + $other_regions;
}

/**
 * Prepare the values passed to the theme_page function to be passed into a
 * pluggable template engine.
 *
 * It's redefined here due to heavy customising.
 */
function lumen_page($content, $show_blocks = TRUE) {
  
  if (theme_get_setting('toggle_favicon')) {
    drupal_set_html_head('<link rel="shortcut icon" href="'. check_url(theme_get_setting('favicon')) .'" type="image/x-icon" />');
  }

  $vars['mission'] = drupal_is_front_page() ? filter_xss_admin(theme_get_setting('mission')) : '';

  // Populate sidebars.
  $vars['show_blocks'] = $show_blocks;
  $vars['layout'] = 'none';
  if ($show_blocks) {
    global $sidebar_indicator;
    // Sidebar_indicator tells the block counting code to count sidebars separately.
    $sidebar_indicator = 'left';
    $vars['sidebar_left'] = theme('blocks', 'left');
    if ($vars['sidebar_left'] != '') {
      $vars['layout'] = 'left';
    }
    $sidebar_indicator = 'right';
    $vars['sidebar_right'] = theme('blocks', 'right');
    if ($vars['sidebar_right'] != '') {
      $vars['layout'] = ($vars['layout'] == 'left') ? 'both' : 'right';
    }
  }
  
  // Construct page title
  if ($vars['title'] = strip_tags(drupal_get_title())) {
    $vars['head_title'] = $vars['title'] .' :: '. variable_get('site_name', 'Drupal');
  }
  else {
    $vars['head_title'] = variable_get('site_name', 'Drupal');
    if (variable_get('site_slogan', '')) {
      $vars['head_title'] .= ' :: '. variable_get('site_slogan', '');
    }
  }
  
  // logo variable originally a path only. Passed to theme_image for image dimensions.
  $vars['logo'] = theme_get_setting('toggle_logo') ? theme('image',
    // Remove base path since theme_image won't take it.
    substr(theme_get_setting('logo'), strlen(base_path())),
    t('logo'),
    variable_get('site_name', 'Drupal'),
    array('class' => 'logo')
  ) : '';
  $vars['logo_linked'] = $vars['logo'] ? l($vars['logo'], '<front>', array(), NULL, NULL, FALSE, TRUE) : '';
  
  // Process style sheets.
  $vars['css']       = lumen_override_css(drupal_add_css());
  // lumen_alt_data() provides override values. lumen_node_bits('tax') provides additive styles.
  $vars['css_lumen'] = lumen_add_theme_styles(lumen_alt_data(), lumen_node_bits('tax'));
  $vars['styles']    = drupal_get_css($vars['css']) . drupal_get_css($vars['css_lumen']) . lumen_get_css();

  // For jquery.lumen.js
  if (file_exists($default_js = path_to_theme() .'/jquery.lumen.js')) {
    drupal_add_js($default_js, 'theme'); // Load default script.
  }
  $vars['host']              = $_SERVER['HTTP_HOST'];
  $vars['r_server_name']     = implode('.', array_reverse(explode('.', $_SERVER['SERVER_NAME'])));
  $vars['minimum_word_size'] = variable_get('minimum_word_size', 3);
  $vars['scripts']           = drupal_get_js();

  // For Search box.
  $vars['search_box']        = theme_get_setting('toggle_search') ? drupal_get_form('search_theme_form') : '';
  // Empty search box when inside the search page.
  if (arg(0) == 'search' && $vars['search_box']) $vars['search_box'] = '';

  // Other visible elements.
  $vars['breadcrumb']        = theme('breadcrumb', drupal_get_breadcrumb());
  $vars['breadcrumb_wtitle'] = theme('breadcrumb', drupal_get_breadcrumb(), TRUE);
  $vars['content']           = $content;
  $vars['feed_icons']        = drupal_get_feeds();
  $vars['footer_message']    = filter_xss_admin(variable_get('site_footer', FALSE));
  $vars['site_slogan']       = theme_get_setting('toggle_slogan') ? variable_get('site_slogan', '') : '';
  $vars['site_name_linked']  = theme_get_setting('toggle_name') ? l(variable_get('site_name', 'Drupal'), '<front>') : '';
  $vars['site_name']         = theme_get_setting('toggle_name') ? variable_get('site_name', 'Drupal') : '';
  $vars['secondary_links']   = theme('links', menu_secondary_links(), array('class' =>'links site-nav secondary-nav'));
  $vars['primary_links']     = theme('links', menu_primary_links(), array('class' =>'links site-nav primary-nav'));
  $vars['messages']          = theme('status_messages');
  $vars['help']              = theme('help');
  $vars['tabs']              = theme('menu_local_tasks');
  
  // Other variables.
  $vars['base_path']         = base_path();
  $vars['closure']           = theme('closure');
  $vars['head']              = drupal_get_html_head();
  $vars['language']          = $GLOBALS['locale'];
  $vars['alt_data']          = lumen_alt_data();

  
  if ((arg(0) == 'node') && is_numeric(arg(1))) {
    $vars['node'] = node_load(arg(1));
  }
  
  // Build a list of suggested template files in order of specificity. One suggestion is made for every
  // element of the current path, though numeric elements are not carried to subsequent suggestions.
  //
  // Suggestions are also made from taxonomy. A term ID has higher specificity than a vocabulary ID.
  // Term and vocabulary weight also plays a role in the loading order but vocabularies will never come
  // before a term. Front page takes precedence, then anything with a numeric ID from the current path.
  // After that, all the associated taxonomy then everything else from the loaded path.
  // For example, "http://www.example.com/node/1/edit" with a term ID of 2 attached to a parent vocab ID
  // of 3 would result in the following suggestions:
  //
  // page-node-1.tpl.php
  // page-node-term-2.tpl.php
  // page-node-vocab-3.tpl.php
  // page-node-edit.tpl.php
  // page-node.tpl.php
  // page.tpl.php
  //
  // Look at the notes on lumen_alt_data() for more information.
  $suggestion = 'templates_suggestion/page';
  $suggestions = array($suggestion);
  foreach (array_reverse($vars['alt_data']) as $data) {
    $suggestions[] = $suggestion .'-'. $data;
  }
  
  return _phptemplate_callback('page', $vars, $suggestions);
}

/*
 * Prepare the values passed to the theme_node function to be passed into a
 * pluggable template engine.
 *
 * It's redefined here due to heavy customising.
 */
function lumen_node($node, $teaser = 0, $page = 0) {
  global $user;
  
  // Node information used for any node dependant manipulations. Initially set here with lumen_node_bits().
  $node_bits = array(
    'uid'  =>  $node->uid,
    'type' =>  $node->type,
    'tax'  =>  lumen_alt_taxonomy($node->taxonomy),
  );
  lumen_node_bits($node->nid, $node_bits); // Store the bits.
  $vars['tax_list'] = $node_bits['tax']; // Needed to create classes in _phptemplate_variables() $hook == 'node'.
  
  // Load the node block region only if it's a page. See lumen_regions().
  $vars['node_region'] = !$teaser ? theme('blocks', 'node_region') . theme('blocks', 'node_region_'. $node->type) : '';

  // Display info only on certain node types.
  if (theme_get_setting('toggle_node_info_' . $node->type)) {
    $vars['submitted'] = theme('username', $node) .' &rsaquo; '. format_date($node->created);
    $vars['picture'] = theme_get_setting('toggle_node_user_picture') ? theme('user_picture', $node) : '';
  }
  else {
    $vars['submitted'] = '';
    $vars['picture'] = '';
  }
  
  // Time based variables.
  $c_time = time();
  // Show relative creation time only when the node exists.
  $vars['r_created'] = $node->nid ? format_interval($c_time - $node->created) : '';
  // Show time difference only when edited after a minute of being created.
  $vars['r_changed'] = ($node->changed - $node->created) >= 60 ? format_interval($c_time - $node->changed) : '';
  $vars['date']      = format_date($node->created);
  
  $vars['node']      = $node; // Pass the actual node to allow more customization.
  $vars['teaser']    = $teaser;
  $vars['page']      = $page;
  $vars['content']   = ($teaser && $node->teaser) ? $node->teaser : $node->body;
  $vars['taxonomy']  = module_exists('taxonomy') ? taxonomy_link('taxonomy terms', $node) : array();
  $vars['terms']     = theme('links', $vars['taxonomy'], array('class' => 'links term-links inline'));
  $vars['links']     = $node->links ? theme('links', $node->links, array('class' => 'links node-links')) : '';
  $vars['name']      = theme('username', $node);
  $vars['node_url']  = url('node/'. $node->nid);
  $vars['title']     = check_plain($node->title);
  
  // Flatten the node object's member fields.
  $vars = array_merge((array)$node, $vars);
  
  // Process suggestions.
  $suggestion = 'templates_suggestion/node';
  $suggestions = array(
    $suggestion,
    "$suggestion-$node->type",
  );
  // Reversed to maintain order.
  foreach (array_reverse($vars['tax_list']) as $tax_suggest) {
    $suggestions[] = "$suggestion-$tax_suggest";
  }
  if ($node->nid) {
    $suggestions[] = "$suggestion-$node->nid";
  }
  // Make teaser suggestions. Doubles the number of suggestions. Could it slow things down?
  if ($teaser) {
    foreach ($suggestions as $suggest_4teaser) {
      $suggestions_wteasers[] = $suggest_4teaser;
      $suggestions_wteasers[] = $suggest_4teaser .'-teaser';
    }
    $suggestions = $suggestions_wteasers;
  }
  
  return _phptemplate_callback('node', $vars, $suggestions);
}

/**
 * Prepare the values passed to the theme_comment function to be passed into a
 * pluggable template engine.
 *
 * It's redefined here due to heavy customising.
 */
function lumen_comment($comment, $links = 0) {
  
  // $comment->nid == node id this comment is attached to.
  $node_bits = lumen_node_bits($comment->nid);

  $vars['author']      = theme('username', $comment);
  $vars['comment']     = $comment;
  $vars['content']     = $comment->comment;
  $vars['date']        = format_date($comment->timestamp);
  $vars['links']       = $links ? theme('links', $links, array('class' => 'links comment-links')) : '';
  $vars['new']         = $comment->new ? t('new') : '';
  $vars['node_uid']    = $node_bits['uid'];
  $vars['node_type']   = $node_bits['type'];
  $vars['picture']     = theme_get_setting('toggle_comment_user_picture') ? theme('user_picture', $comment) : '';
  $vars['submitted']   = '&lfloor; '. theme('username', $comment) .' - '. format_date($comment->timestamp);
  $vars['title']       = l($comment->subject, $_GET['q'], NULL, NULL, "comment-$comment->cid");
  
  $suggestion = 'templates_suggestion/comment';
  $suggestions = array(
    $suggestion,
    $suggestion .'-'. $node_bits['type'],
  );
  
  return _phptemplate_callback('comment', $vars, $suggestions);
}

/**
 * Prepare the values passed to the theme_block function to be passed
 * into a pluggable template engine. Uses block properties to generate a
 * series of template file suggestions. If none are found, the default
 * block.tpl.php is used.
 *
 * It's redefined here due to heavy customising.
 */
function lumen_block($block) {
  $vars['region'] = str_replace('_', '-', $block->region);
  $vars['module'] = str_replace('_', '-', $block->module);
  $vars['block']  = $block;
  
  $suggestion = 'templates_suggestion/block';
  $suggestions = array(
    $suggestion,
    "$suggestion-". $vars['region'],
    "$suggestion-". $vars['module'],
    "$suggestion-". $vars['region'] .'-'. $vars['module'],
    "$suggestion-". $vars['module'] .'-'. $block->delta,
  );
  if (drupal_is_front_page()) {
    $suggestions[] = "$suggestion-front";
  }
  
  return _phptemplate_callback('block', $vars, $suggestions);
}



/* #################### THEME OVERRIDES CORE #################### */

/* ========== "includes/form.inc" ========== */

/**
 * Fixes illegal duplicate html id's. id's of "edit-sumit" found in:
 * theme_comment_controls(), theme_search_theme_form(), and
 * theme_search_block_form(). To-do: remove when fixed in core.
 */
function phptemplate_submit($element) {
  static $dupe_ids = array();
  // Prevent duplicate id's.
  if (isset($dupe_ids[$element['#id']])) {
    $dupe_ids[$element['#id']] = $dupe_ids[$element['#id']] + 1;
    $element['#id'] = $element['#id'] .'-'. $dupe_ids[$element['#id']];
  }
  else {
    $dupe_ids[$element['#id']] = 0;
  }
  return theme('button', $element); 
}


/* ========== "includes/menu.inc" ========== */

/**
 * Generate the HTML output for a single menu item.
 *
 * Modified to set an "active-trail" class through a menu hierarchy. Added to
 * <li> elements when a descendant link is in an active state.
 *
 * @param $mid
 *   The menu id of the item.
 * @param $children
 *   A string containing any rendered child items of this menu.
 * @param $leaf
 *   A boolean indicating whether this menu item is a leaf.
 */
function phptemplate_menu_item($mid, $children = '', $leaf = TRUE) {
  if ($leaf) {
    $classes = 'leaf';
  }
  else {
    $classes = $children ? 'expanded' : 'collapsed';
  }
  $classes .= in_array($mid, _menu_get_active_trail()) ? ' active-trail' : '';    
  $output = _lumen_callback('menu_item',
    array(
      'children'  =>  $children,
      'classes'   =>  $classes,
      'leaf'      =>  $leaf,
      'menu_link' =>  menu_item_link($mid),
    )
  );
  return $output ? $output : theme_menu_item($mid, $children, $leaf);
}


/* ========== "includes/theme.inc" ========== */

/**
 * Return a themed breadcrumb trail.
 *
 * Added $title property.
 *
 * @param $breadcrumb
 *  An array containing the breadcrumb links.
 * @param $title
 *  (optional) Boolean to make the page title a part of the bread crumb.
 */
function phptemplate_breadcrumb($breadcrumb, $title = FALSE) {
  if (!empty($breadcrumb)) {
    $output = _lumen_callback('breadcrumb',
      array(
        'breadcrumb'  =>  $breadcrumb,
        'title'       =>  $title ? l(strip_tags(drupal_get_title()), $_GET['q'], array('class' => 'title')) : '',
      )
    );
    return $output ? $output : theme_breadcrumb($breadcrumb);
  }
  return '';
}

/**
 * Prevent empty paragraph tags from giving a false positive. Also fixes some
 * inconsistencies in contrib modules.
 *
 * To-do: Remove when it's all sorted out.
 */
function phptemplate_help() {
  if ($help = menu_get_active_help()) {
    if (strpos($help, '<p>') === FALSE) {
      $help = '<p>'. $help .'</p>';
    }
    return ($help != "<p></p>\n") ? '<div class="help">'. $help .'</div>' : '';
  }
}

/**
 * Return a themed sort icon.
 *
 * Changed arrow image and added class of "tablesort-img".
 *
 * @param $style
 *  Set to either asc or desc. This sets which icon to show.
 * @return
 *  A themed sort icon.
 */
function phptemplate_tablesort_indicator($style) {
  $path = path_to_theme() .'/images/'. ($style == 'asc' ? 'ar2d.gif' : 'ar2u.gif');
  $title_attr = $style == 'asc' ? t('sort ascending') : t('sort descending');
  return ' '. theme('image', $path, t('sort icon'), $title_attr, array('class' => 'tablesort-img')); 
}

/**
 * Return a set of blocks available for the current user.
 *
 * Modified to prevent phptemplate from invoking regions outside of the 'page'
 * scope. It does this by checking the global variable $block_filter_list.
 * Initially set within lumen_regions() so the filter is available after the
 * function is called. The most common chain of functions that lead to
 * lumen_regions() is: [ theme_page > _phptemplate_callback >
 * _phptemplate_default_variables > system_region_list > lumen_regions ].
 * Phptemplate assumes all blocks should be output from page.tpl.php.
 * Something this theme doesn't do.
 *
 * Since it's called very late, (theme_page to render all the collected data)
 * the filter will not apply for preceding calls. To-do: Maybe improve this.
 *
 * @param $region
 *   Which set of blocks to retrieve.
 * @return
 *   A string containing the themed blocks for this region.
 */
function phptemplate_blocks($region) {
  global $block_filter_list;
  
  if (isset($block_filter_list[$region])) {
    return '';
  }
  
  $output = '';
  if ($list = block_list($region)) {
    foreach ($list as $key => $block) {
      // $key == <i>module</i>_<i>delta</i>
      $output .= theme('block', $block);
    }
  }
  // Add any content assigned to this region through drupal_set_content() calls.
  $output .= drupal_get_content($region);
  
  return $output;
}


/* ========== "modules/comment/comment.module" ========== */

/**
 * Title of comment controls changes depending on the type of parent node.
 *
 * Disabled controls for anonymous users when cache is turned on. Doesn't
 * function in that state. To-do: remove when fixed in core.
 */
function phptemplate_comment_controls($form) {
  global $user;
  // Disable for anonymous users when caching is enabled since it's not functional.
  if (($user->uid == 0) && (variable_get('cache', '0') != '0')) {
    // Must return FALSE, otherwise it's still partially rendered. Not sure why.
    return FALSE;
  }
  else {
    $node_bits = lumen_node_bits(arg(1));
    $output = _lumen_callback('comment_controls',
      array(
        'content'     =>  drupal_render($form),
        'description' =>  t('Select your preferred way to display the !type and click "Save settings" to activate your changes.', array('!type' => $node_bits['type'] == 'forum' ? t('forum') : t('comments'))),
      )
    );
    return $output ? $output : theme_comment_controls($form);
  }
}

/**
 * Allow themable wrapping of all comments.
 *
 * Comment block region and a second forum topic navigation rendered here. If
 * comments are disabled, it will not show at all.
 */
function phptemplate_comment_wrapper($content) {
  $node_bits = lumen_node_bits(arg(1));
  $comment_region = theme('blocks', 'comment_region') . theme('blocks', 'comment_region_'. $node_bits['type']);
  
  $classes = lumen_make_classes(array(
    $node_bits['type']  =>  TRUE,
    'has-region'        =>  $comment_region ? TRUE : FALSE,
  ), 'comments');
  $output = _lumen_callback('comment_wrapper',
    array(
      'title'               =>  $content ? ($node_bits['type'] != 'forum' ? t('Comments') : '') : '',
      'comment_region'      =>  $comment_region,
      'classes'             =>  $classes,
      'content'             =>  $content,
    )
  );
  
  return $output ? $output : theme_comment_wrapper($content);
}



/* #################### THEME OVERRIDES CONTRIB #################### */

// Place holder.



/* #################### THEME FUNCTIONS CUSTOM #################### */

/**
 * Create an array of classes.
 *
 * Possible values for $info: 'key' => 'value' or 'key' => boolean (1 & 0 as
 * strings or integers). TRUE, 1 or "1" will return the key only. FALSE, 0,
 * "0" or "" -empty string will disable itself and return nothing. Other
 * values will concat the key with the value. e.g. 'keyvalue'. Some items will
 * return a value of a 1 or 0 where it's not intended to be a boolean. In that
 * case, concat a string to the value before it gets passed through here.
 * Example: ('key' => 'string'. $value) No two keys can be the same within a
 * single run or the latter key will overwrite the former.
 *
 * @param $info
 *  Array of attributes to be converted.
 * @param $hook
 *  To prepend when flattening.
 * @param $flatten
 *  Flatten output to a string. Also prepend $hook to each class.
 * @return
 *  Array or string of classes. Dependent on $flatten.
 */
function lumen_make_classes($info, $hook, $flatten = TRUE) {
  $output = array();

  foreach ($info as $key => $value) {
    if ($value == 1) {
      $output[] = $key;
    }
    elseif ($value != (0 || '0')) {
      $output[] = $key . $value;
    }
  }
  
  return $flatten ? "$hook-". implode(" $hook-", $output) : $output;
}

/**
 * Dynamic style sheet loader.
 *
 * The base path is scanned for .css files. Anything found is loaded into
 * lumen_add_css() automatically. If an overriding alternate is found then
 * it overrides the base style. The alternate style detection comes from
 * $alt_override which is an arrary of values that gets combined with the base
 * style name. The first one found is used. Others are ignored.
 *
 * Items in the $alt_additive array are also loaded but it doesn't override.
 * As the name suggests, it loads all matching styles when it exists.
 *
 * The $alt_override is fed taxonomy and path information by default. You can
 * pass any list you want. $alt_additive is fed only taxonomy information.
 *
 * @param $alt_override
 *  Array of values to override defaults.
 * @param $alt_additive
 *  Array of values cumulatively load styles.
 * @param $alt_dir
 *  Directory for alternates. If none is set then defaults are used.
 * @param $args
 *  A keyed array of arguments to be passed to lumen_add_css(). Key must match
 *  a base style (minus .css extension) or string from a matching item from
 *  $alt_additive.
 * @return
 *  Array of styles information ready for drupal_get_css().
 */
function lumen_add_theme_styles($alt_override = array(), $alt_additive = array(), $alt_dir = '', $args = array()) {
  $theme_dir = path_to_theme();
  $base_dir = "$theme_dir/styles";
  $alt_dir = $alt_dir ? $alt_dir : "$theme_dir/styles_alt";
  
  // Get all the default styles.
  foreach (glob("$base_dir/*.css") as $file) {
    $css_defaults[basename($file, '.css')] = $file;
  }
  // Process overrides. Only the first hit will override defaults.
  foreach ($css_defaults as $component => $path) {
    foreach ($alt_override as $override) {
      if (file_exists($file = $alt_dir .'/'. $component .'_'. $override .'.css')) {
        $css_queue[$component] = $file;
        // Break out of loop on first hit.
        break;
      }
    }
    // Use default when no alternates are found.
    if (!isset($css_queue[$component])) {
      $css_queue[$component] = $path;
    }
  }
  // Process additive styles. All matching hits will be loaded.
  foreach (array_unique($alt_additive) as $additive) {
    if (file_exists($file = $alt_dir .'/'. $additive .'.css')) {
      $css_queue[$additive] = $file;
    }
  }
    
  // Combine arguments. Keyed so the ones set here can be overriden.
  $args = $args + array(
    'print'   => array('linked', 'print'),
    'ie5-6'   => array('linked:if lte IE 6'),
  );
  foreach ($css_queue as $component => $file) {
    $this_args = isset($args[$component]) ? array_merge(array($file), $args[$component]) : array($file);
    call_user_func_array('lumen_add_css', $this_args);
  }
  
  return lumen_add_css();
}

/**
 * Replace 'module' styles when an override exists in "styles_module/"
 * directory in the theme folder. It can also be disabled by appending
 * ".disabled" to the style name. Style order is maintained.
 *
 * @param $css
 *  Array containing css information.
 * @param $override_dir
 *  The path to look to check. If none is set then a default is used.
 * @return
 *  Array with altered css information.
 */
function lumen_override_css($css, $override_dir = '') {
  $css_override = array();  
  $override_dir = $override_dir ? $override_dir : path_to_theme() .'/styles_module/';
  
  foreach ($css as $media => $types) {
    foreach ($types as $type => $files) {
      if ($override_type != $type) {
        foreach ($files as $file => $preprocess) {
          // Get the base name since the style could be located in multiple locations.
          if (file_exists($override = $override_dir . basename($file))) {
            $css_override[$media][$type][$override] = $preprocess;
          }
          // Appending ".disabled" to the style disables it. Useful if you want to manage the styles
          // from the theme style. IF you do this, it's recommened to place the styles at the top.
          elseif (!file_exists($override .'.disabled')) {
            $css_override[$media][$type][$file] = $preprocess;
          }
        }
      }
    }
  }
  if (!empty($css_override)) {
    $css = $css_override;
  }
  
  return $css;
}

/**
 * Adds inline or linked CSS to the stylesheet queue. -Independant from core.
 *
 * Works in two ways. The default is to have a $type of 'linked'. When
 * unchanged, the array of links are structured for drupal_get_css() for
 * further processing. The other is to use an alternate $type.
 * Can be 'inline', 'inline:if IE CONDITION' or 'linked:if IE CONDITION'.
 * example: lumen_add_css('ie6-style.css', 'linked:if lte IE 6');
 *
 * Only supports downlevel-hidden conditional comments. Downlevel-revealed
 * are less useful and break validation. More information found here:
 * http://msdn.microsoft.com/workshop/author/dhtml/overview/ccomment_ovw.asp
 *
 * @param $data
 *  (optional) url to the style sheet or inline CSS. Depends on $type.
 * @param $type
 *  (optional) string: 'linked', 'inline', 'inline:if IE 5', etc...
 * @param $media
 *  (optional) The media type for the stylesheet, e.g. all, print, screen.
 * @param $preprocess
 *  (optional) Should this CSS file be aggregated and compressed if this
 *  feature has been turned on under the performance section?
 * @return
 *  An array of CSS files. Depends on $type.
 */
function lumen_add_css($data = NULL, $type = 'linked', $media = 'all', $preprocess = TRUE) {
  static $css = array('core format' => array(), 'custom format' => array());
  
  if (isset($data)) {
    if ($type == 'linked') { // Default condition. Exact match.
      $css['core format'][$media]['theme'][$data] = $preprocess; // Structured for drupal_get_css().
    }
    else {
      // How is the css attached? $type string must *contain* 'inline' or 'linked'. Doesn't need to be exact. 
      $attach = strpos($type, 'inline') !== FALSE ? 'inline' : (strpos($type, 'linked') !== FALSE ? 'linked' : '');
      if ($attach) {
        if (!isset($css['custom format'])) { // Set default order if not already set.
          $css['custom format'] = array('linked' => array(), 'inline' => array('none' => array()));
        }
        $ie_cond = ($pos = strpos($type, ':')) !== FALSE ? substr($type, $pos + 1) : 'none';
        if (!isset($data, $css['custom format'][$attach][$ie_cond][$media][$data])) { // Prevent duplicates.
          $attach_data = $attach == 'linked' ? '@import "'. base_path() ."$data\";" : "\n  $data";
          $css['custom format'][$attach][$ie_cond][$media][$data] = $attach_data;
        }
      }
    }
  }
  
  return $type == 'linked' ? $css['core format'] : $css['custom format'];
}

/**
 * Renders inline or linked style tags. Complimentary to lumen_add_css().
 */
function lumen_get_css() {
  $output = '';
  $css = lumen_add_css(NULL, 'inline');
  
  foreach ($css as $attach => $ie_conds) {
    foreach ($ie_conds as $ie_cond => $medias) {
      $output .= $ie_cond != 'none' ? "<!--[$ie_cond]>\n" : '';
      foreach ($medias as $media => $datas) {          
        $output .= "<style type=\"text/css\" media=\"$media\">";
        foreach ($datas as $data) {
          $output .= $data;
        }
        $output .= ($attach == 'inline' ? "\n" : "" ) ."</style>\n";
      }
      $output .= $ie_cond != 'none' ? "<![endif]-->\n" : '';
    }
  }
  
  return $output;
}

/**
 * Holds node data so it can be passed around to various functions. Supplying
 * a numeric node id and data ($bits) will store it. Calling the function with
 * the node id alone will retrieve everything for the node. Calling it with a
 * key will return all associated information across all node ids. And calling
 * it with no parameters will return everything stored.
 *
 * @param $key
 *  String or integer, used to store and retrieve.
 * @param $bits
 *  (optional) Mainly used to store when $key is a node id.
 */
function lumen_node_bits($key = NULL, $bits = array()) {
  static $node_bits = array();
  $output = array();
  
  if (is_numeric($key)) {
    if (!empty($bits)) {
      $node_bits[$key] = isset($node_bits[$key]) ? array_merge($node_bits[$key], $bits) : $bits;
    }
    else {
      $output = isset($node_bits[$key]) ? $node_bits[$key] : FALSE;
    }
  }
  else {
    foreach ($node_bits as $nid => $bits) {
      if (isset($bits[$key])) {
        if (is_array($bits[$key])) {
          $output = array_merge($output, $bits[$key]);
        }
        else {
          $output[$nid] = $bits[$key];
        }
      }
    }
    if (empty($output)) {
      $output = $node_bits;
    }
  }
  
  return $output;
}

/**
 * Generates data based on path and node taxonomy when available. The output
 * is sorted based on specificity. Taxonomy order based on weight and terms
 * always come before vocabulary. Look at lumen_alt_taxonomy().
 *
 * Information generated here is useful for supplying alternate data for
 * loading style sheets and template naming suggestions. It's also used for
 * page classes.
 */
function lumen_alt_data() {
  static $output = array();
  
  if (empty($output)) {
    // Separated to maintain proper order.
    $group_a = array();
    $group_b = array();
    $group_c = array();
    
    if (drupal_is_front_page()) {
      $group_a[] = 'front';
    }
    // Check that we are dealing with a full page node.
    if (arg(0) == 'node' && is_numeric(arg(1)) && $node_bits = lumen_node_bits(arg(1))) {
      foreach ($node_bits['tax'] as $taxonomy) {
        $group_b[] = $taxonomy;
      }
      $group_b[] = $node_bits['type'];
    }
    // Filter so we don't get search terms.
    if (arg(0) == 'search') {
      $group_c[] = 'search';
      $group_c[] = 'search-'. arg(1);
    }
    else {
      // Prevent the *default* front page setting of "node" from getting processed
      // since it's just a *list of nodes* and not a specific node.
      if (!(arg(0) == 'node' && !arg(1))) {
        $i = 0;
        $prev_args = '';
        while ($arg = arg($i++)) {
          if (is_numeric($arg)) {
            $group_a[] = $prev_args .'-'. $arg;
            $prev_args = ''; // Reset when numeric.
          }
          elseif ($prev_args) {
            $group_c[] = $prev_args .'-'. $arg;
            $prev_args .= '-'. $arg;
          }
          else {
            $group_c[] = $arg;
            $prev_args = $arg;
          }
        }
      }
    }
    $output = array_merge($group_a, $group_b, $group_c);
  }
  
  return $output;
}

/**
 * Returns data which aids in altering page presentation dependent on
 * taxonomy.
 *
 * Numeric vocabulary and term id's are used instead of names since they can
 * overlap. It could also add more complexity from name cleaning.
 *
 * @param $taxonomy
 *  Array of taxonomy values. Structure depends on viewing state,
 *  e.g. normal view vs. preview from edits.
 * @return
 *  Array of term and vocabulary id's.
 */
function lumen_alt_taxonomy($taxonomy) {
  $output = array();
  
  // Make sure we have new values.
  if (module_exists('taxonomy') && !empty($taxonomy)) {
    
    // Process into a useable structure.
    _lumen_alt_taxonomy($taxonomy);
    
    foreach ($taxonomy as $vocab => $terms) {
      $output_v[] = 'vocab-'. $vocab;
      foreach ($terms as $term) {
        $output_t[] = 'term-'. $term;
      }
    }
    $output = isset($output_v) ? array_merge($output_t, $output_v) : array();
  }
  
  return $output;
}

/**
 * Helper function for lumen_alt_taxonomy(). Converts $taxonomy array into
 * a consistent form.
 *
 * Complexity due to differing array structure of taxonomy in normal view vs.
 * preview. This is necessary in order to preview the changes while editing.
 * Preview state is detected from internal paths, e.g. arg(#) function.
 */
function _lumen_alt_taxonomy(&$taxonomy) {
  
  // Taxonomy structure different depending on view state. Rearrange for consistent output.
  if (arg(0) == 'node' && (is_numeric(arg(1)) && arg(2) == 'edit' || arg(1) == 'add')) {
    // Free tagging vocabularies do not send their tids in the edit form,
    // so we'll detect them here and process them into a useable structure.
    // Copied from core, taxonomy.module,v 1.329 2006/12/20 10:32:16 -> taxonomy_node_save().
    if (isset($taxonomy['tags'])) {
      $typed_input = $taxonomy['tags'];
      unset($taxonomy['tags']);
      foreach ($typed_input as $vid => $vid_value) {
        // This regexp allows the following types of user input:
        // this, "somecmpany, llc", "and ""this"" w,o.rks", foo bar
        $regexp = '%(?:^|,\ *)("(?>[^"]*)(?>""[^"]* )*"|(?: [^",]*))%x';
        preg_match_all($regexp, $vid_value, $matches);
        $typed_terms = array_unique($matches[1]);
        $inserted = array();
        foreach ($typed_terms as $typed_term) {
          // If a user has escaped a term (to demonstrate that it is a group,
          // or includes a comma or quote character), we remove the escape
          // formatting so to save the term into the database as the user intends.
          $typed_term = str_replace('""', '"', preg_replace('/^"(.*)"$/', '\1', $typed_term));
          $typed_term = trim($typed_term);
          if ($typed_term == "") {
            continue;
          }
          // See if the term exists in the chosen vocabulary and set proper array structure..
          $possibilities = taxonomy_get_term_by_name($typed_term);
          $typed_term_tid = NULL; // tid match, if any.
          foreach ($possibilities as $possibility) {
            if ($possibility->vid == $vid) {
              $taxonomy[$possibility->vid] = array($possibility->tid => $possibility->tid);
            }
          }
        }
      }
    }
    // Prepare for consistency and clean out zero values.
    foreach ($taxonomy as $vocab => $terms) {
      // Multi-select & multiple freetags have multiple terms, i.e. array.
      if (is_array($terms)) {
        foreach ($terms as $term) {
          // <none> selection hold a value of '0' -integer. Filter out.
          if ($term == FALSE) {
            unset($taxonomy[$vocab][$term]);
          } 
        }
      }
      // Single-select are not arrays. Convert to an array for consistency.
      // <none> selection hold a value of '0' -integer. Filter out.
      elseif ($terms != FALSE) {
        $taxonomy[$vocab] = array($terms => $terms);
      }
      // If the vocabulary is empty, filter out.
      if (empty($taxonomy[$vocab])) {
        unset($taxonomy[$vocab]);
      }
    }
  }
  else {
    // Resturcture so Vocabulary (vid) is parent of $term (tid).
    foreach ($taxonomy as $term => $term_info ) {
      $vid2parent[$term_info->vid][$term] = $term_info->tid;
    }
    $taxonomy = $vid2parent;
  }
  
}

/**
 * Simplified version of _phptemplate_callback().
 *
 * This function is for use only on theme function overrides. Slimmed down to
 * speed up execution. Defaults to files inside "/templates_overrides/".
 *
 * @return
 *  Rendered HTML if file is found. Otherwise returns FALSE.
 */
function _lumen_callback($hook, $vars = array()) {
  $file = path_to_theme() .'/templates_override/'. $hook .'.tpl.php';  
  return file_exists($file) ? _phptemplate_render($file, $vars) : '';
}
