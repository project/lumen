<div class="box">

  <?php if ($title) : ?>
  <h3 class="title">
    <?php print $title ?>
  </h3>
  <?php endif; ?>
  
  <?php print $content // Box content ?>
  
</div>
