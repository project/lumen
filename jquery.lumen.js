
jQuery.fn.extend({
  // Create a column view from ul tree.
  column_view : function() {
    this.each(function() { // Each top level ul.
      $(this).wrap('<div class="column-view clear-block"></div>')
        .find('ul').each(function(n) { // Each child ul.
        // Get active trail. To-do: this may break if there's more than one active trail branching off in different directions.
        var active = $('a.active', this.parentNode).length > 0 ? true : false;
        $(this.parentNode).attr('match', 'match-'+n) // Custom attribute to match parent > child on click.
          .addClass('has-child'+(active?' active':'')); // Set 'has-child' class to filter clicks. 'active' class for active trail.
        $(this).wrap('<div class="match-'+n+'" style="display:'+(active?'block':'none')+';overflow:hidden;"></div>')
          .parents('div:eq(1)').append($(this.parentNode)); // Appends the above wrap to ".parents('div:eq(1)')" -second div outwards.
      });
      
      // Divide .column-view width by the number of visible columns then set the width of columns.
      $('ul', this.parentNode).width(($(this.parentNode).width() / ++$('div:visible', this.parentNode).length)+'px')
        .find('> li.has-child').click(function(e) {
          e.preventDefault(); // Prevent default click-through of child anchor.
          // Hide & show matching elements. 'div:eq(0)' = closest div parent from point of click.
          $(this).parents('div:eq(0)').find('div:visible').hide().end().find('div.'+$(this).attr('match')).show();
          $(this).parents('div.column-view:eq(0)').each(function() { // Get into scope for div.column-view.
            var num_c = ++$('div:visible', this).length; // Number of visible columns.
            var new_w = $(this).width() / num_c; // Divide .column-view width by the number of visible columns.
            // Check the width of the first column with the last. Make sure they are equal to prevent un-equal column widths.
            // Get difference between the old width and new. Used to prevent animations when there are no changes.
            var col_1 = $('> ul', this).width(); // First column.
            var diff = (col_1 == $('div:visible:eq('+(num_c - 2)+') > ul', this).width() ? col_1 : 0) - new_w;
            // Give +/- 1 margin of tolerance to prevent un-neccessary animations. More columns the faster the animation.
            if (diff < -1 || diff > 1) { $('ul', this).animate({width:new_w}, (500 / num_c) + 123); }
          });
            
          $(this).addClass('focus').siblings('li.focus').removeClass('focus');  // Add focus class for styling.
        })
          .find('> a').dblclick(function() { location.href = this.href; }); // Double clicking loads .has-child links.
    });
    
    return $(this).parent(); // Returns div.column-view object.
  },
  // Adds a class to the first occurring element and character for styling.
  leader : function() {
    $(this).each(function() {
      if ($(this).siblings('.leader').length == 0) {
        $(this).addClass('leader');
        var first = this.innerHTML.substr(0,1);
        if (/[a-zA-Z0-9]/.test(first)) {
          var node = this.firstChild;
          node.nodeValue = node.nodeValue.slice(1);
          // "first-c" = first character. Each character gets a class also for possible kerning.
          // It's recommended you set rule for all upper or lowercase if you do any letter spacing adjustments. (kerning)
          // Note: the classes for each character are all lowercase.
          $('<span class="first-c fc-'+first.toLowerCase()+'">'+first+'</span>').prependTo(this);
        }
      }
    });
    
    return this;
  },
  // Mark external links and add class.
  mark : function(settings) {
    settings = jQuery.extend({ classes: '', symbol: '' }, settings);
    this.each(function() {
      if (settings.classes) { $(this).addClass(settings.classes); }
      if (settings.symbol) { $(this).append('<span class="marker">'+settings.symbol+'</span>'); }
    });
    
    return this;
  },
  // Use the webkit search box for Safari. Mimic in other browsers.
  alter_search : function(min_chars, r_server_name, width) {
    var match = eval('/[a-zA-Z0-9]{'+min_chars+',}/g');
    
    this.each(function() {
      
      if (jQuery.browser.safari) { // Safari will only run this.
        $('input:text', this).attr('type', 'search').attr('size', width)
          .attr('results', '10').attr('autosave', r_server_name + '.search_history')
          .filter(':eq(0)').attr('placeholder', $('input:submit:eq(0)', this).val());
      }
      else { // All other browsers will mimic Safari.
        
        var field_val = $('input:text:eq(0)', this).val();
        var button_val = $('input:submit:eq(0)', this).val();
        // Version 1.0.x of jQuery returns empty string. 1.1.x returns null.
        var was_empty = field_val == '' || field_val == null || field_val == button_val ? true : false;
        
        $('input:text', this).attr('size', width).before('<span class="sbox-l"></span>')
          .after('<span class="sbox-r"></span>').wrap('<span class="sbox"></span>');
        
        if (was_empty) {
          $('input:text:eq(0)', this).addClass('place-holder').attr('value', button_val);
        }
        else {
          $('.sbox-r:eq(0)', this).addClass('can-clear');
        }
        
        $(this)._behave_like_safari(button_val);
      }
      $('input:submit:eq(0)', this).remove();
      // Annoying clear-block generates scroll bars in advanced search form.
      $('div.action', this).removeClass('clear-block');
      $('#edit-category', this).parents('.criterion').attr('id', 'criterion-category');
      /*$('input:text', this).keyup(function(e) {
        if ($(this).val().match(match) != null) {
          $(this).parents('.container-inline:eq(0)').find('button:eq(0)').animate({width:'toggle'}, 'slow');
          if (e.keyCode == 13) {
            searchform.submit();
          }
        }
        else {
          $(this).parents('.container-inline:eq(0)').find('button:eq(0)').animate({width:'toggle'}, 'fast');
          if (e.keyCode == 13) {
          
          }
        }
      });*/
    });//.submit(function(e) { e.preventDefault(); });
    
    return this;
  },
  // Helper function for alter_search.
  _behave_like_safari : function(place_holder) {
        
    this.submit(function(e) { // Prevent place holder from being part of advanced search submission.
      if ($('input:text:eq(0)', this).attr('value') == place_holder) {
        $('input:text:eq(0)', this).attr('value', '');
      }
    });
    
    $('input:text', this).focus(function() {
      // Clear values and set color when it's the place holder.
      if ($(this).attr('value') == place_holder) {
        $(this).attr('value', '').removeClass('place-holder');
      }
    })
    .blur(function() {
      // If value is the place holder or if it's empty.
      if ($(this).attr('value') == place_holder || !$(this).attr('value')) {
        // Only if it's the first input field. Prevents advanced search settings from being affected.
        if ($(this).attr('name') == $(this).parents('form').find('input:text:eq(0)').attr('name')) {
          $(this).addClass('place-holder').attr('value', place_holder);
        }
      }
    })
    .keyup(function(e) {
      if ($(this).attr('value')) { // Has value? Then show clear button.
        $('~.sbox-r', this.parentNode).addClass('can-clear');
      }
      else { // No value? Then hide clear button.
        $('~.sbox-r', this.parentNode).removeClass('can-clear');
      }
    });
    
    $('.sbox-r', this).click(function() { // Clear button click. Clears then focuses on input box.
      $('.sbox input:text', this.parentNode).attr('value', '').get(0).focus();
      $(this).removeClass('can-clear');
    });
    
    $('.sbox-l', this).click(function() { // Magnifying glass click. Selects text.
      $('.sbox input:text', this.parentNode).get(0).select();
    });
    
    return this;
  }
});

jQuery.extend({
  // Various workarounds for IE 6 and lower.
  fix_ie : function() {
    if (navigator.appVersion.match(/MSIE [0-6]\./)) {
      $('a').click(function() { this.blur(); }); // Killing outlines on click not supported through CSS.
      $('*', e).each(function () { // To-do: this isn't working..
        if (this.currentStyle.backgroundImage != 'none') {
          var image = this.currentStyle.backgroundImage;
          image = this.currentStyle.backgroundImage.substring(5, image.length - 2);
          $(this).css({
            'backgroundImage': 'none',
            'filter': "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=crop, src='" + image + "')"
          });
        }
      });
    }
  }
})
