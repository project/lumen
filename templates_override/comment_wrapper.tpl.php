<div id="comments" class="<?php print $classes ?>">
  
  <?php if ($title) { ?>
  <h2 class="title"><?php print $title ?></h2>
  <?php } ?>
  
  <?php print $comment_region // Block region -> block.tpl.php ?>
  
  <hr class="clear hidden" />
  
  <?php print $content // Grouped comments. ?>
  
  <?php print $forum_navigation // doesn't print if there are no comments. ?>
  
</div>
