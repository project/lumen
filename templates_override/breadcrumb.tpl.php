<div class="breadcrumb<?php print $title ? ' breadcrumb-wtitle' : '' ?>">
  <?php print implode(' &rsaquo; ', $breadcrumb) . ($title ? " &rsaquo; <em>$title</em>" : '') ?>
</div>
