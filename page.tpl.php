<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>

  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  
  <?php if (strpos($scripts, "jquery.lumen.js") !== FALSE) : ?>
  <script type="text/javascript">
  //<![CDATA[
    if (Drupal.jsEnabled) {
      $(function(){
        $('#block-user-1.block-header-region > ul').column_view();
        //$('.rounded').round_corners();
        $('#main div.node div.container > p').leader();
        // Filter host since Drupal inconsistently creates absolute links with hrefs for internal links.
        $('#page-bounds a[@href^=http://]:not([@href^=http://<?php print $host ?>])').mark({classes: 'external', symbol: '&#x2303;'});
        $('#search-theme-form, #search-block-form, #search-form').alter_search(<?php print "$minimum_word_size, '$r_server_name'" ?>, 23);
        //$.fix_ie(); // not working yet.
      });
    }
  //]]>
  </script>
  <?php endif; ?>
  
</head>

<body class="<?php print $classes ?>">
  
  <div id="page-bounds"> <!-- START PAGE BOUNDS -->
    
    <div id="site-header">
      <?php if ($logo_linked || $site_name_linked || $site_slogan) : ?>
      <div id="site-id">
        <?php if ($site_name_linked) : ?>
        <h1 class='site-name'>
          <?php print $logo_linked // a img.logo // Use $logo for unlinked version. ?>
          <?php print $site_name_linked // a // Use $site_name for unlinked version. ?>
        </h1>
        <?php endif; ?>
        <?php if ($site_slogan) : ?>
        <p class='site-slogan'><?php print $site_slogan ?></p>
        <?php endif; ?>
      </div>
      <hr class="hidden" />
      <?php endif; ?>
            
      <?php if ($primary_links) : ?>
      <div class="primary-links<?php print $secondary_links ? ' secondary-links' : '' ?>">
        <b class="corners t-corner"><b class="cs"></b></b>
        <div class="container">
          <?php print $primary_links . $secondary_links // ul.links.[primary-links|secondary-links] ?>
        </div>
        <b class="corners b-corner"><b class="cs"></b></b>
      </div>
      <?php endif; ?>
      
      <hr class="clear hidden" />
      
      <?php print $header_region // Block region -> block.tpl.php ?>
    </div>
    
    
    <?php if ($search_box || $breadcrumb) : ?>
    <div id="mid-region">
      <?php print $search_box // form#search-theme-form ?>
      <?php print $breadcrumb // div.breadcrumb ?>
      <hr class="clear hidden" />
    </div>
    <?php endif; ?>
    
    
    <div id="main">
      <?php if ($title || $tabs || $messages) : ?>
      <div id="page-header" class="clear-block">
        <?php print $messages // div.messages.[status|error] ?>
        <h1 class="title"><?php print $title ?></h1>
        <?php print $tabs // ul.tabs.[primary|secondary] ?>
      </div>
      <?php endif; ?>
      
      <?php print $help // div.help ?>
      
      <?php if ($mission) : ?>
      <div id="mission">
        <?php print $mission ?>
        <hr class="clear hidden" />
      </div>
      <?php endif; ?>
      
      <?php print $content // Main content/region ?>
  
      <hr class="clear hidden" />
    </div>
    
    
    <?php if ($sidebar_left) : ?>
    <div id="side-left" class="side">
      <b class="corners"><b class="c1"></b><b class="c2"></b><b class="c3"></b></b>
      <div class="container clear-block">
        <?php print $sidebar_left // Block region -> block.tpl.php ?>
      </div>
      <b class="corners"><b class="c3"></b><b class="c2"></b><b class="c1"></b></b>
    </div>
    <?php endif; ?>
    
    
    <?php if ($sidebar_right) : ?>
    <div id="side-right" class="side">
      <b class="corners"><b class="cs"></b></b>
      <div class="container clear-block">
        <?php print $sidebar_right // Block region -> block.tpl.php ?>
      </div>
      <b class="corners"><b class="cs"></b></b>
    </div>
    <?php endif; ?>
    
    
    <div id="site-footer">
      <?php print $footer_region // Block region -> block.tpl.php ?>
      <?php print $feed_icons // a.feed-icon img ?>
      <?php print $breadcrumb_wtitle // div.breadcrumb.breadcrumb-wtitle ?>
            
      <?php if ($primary_links) : ?>
      <hr class="hidden" />  
      <div class="primary-links<?php print $secondary_links ? ' secondary-links' : '' ?>">
        <b class="corners"><b class="cs"></b></b>
        <?php print $secondary_links . $primary_links // ul.links.[secondary-links|primary-links] ?>
        <b class="corners"><b class="cs"></b></b>
      </div>
      <?php endif; ?>
      
      <hr class="clear hidden" />
    </div>
    
    <b class="falloff-t"></b><b class="falloff-r"></b><b class="falloff-b"></b><b class="falloff-l"></b>
    
  </div> <!-- END PAGE BOUNDS -->


  <?php if ($footer_message) : ?>
  <div id="footer-message">
    <?php print $footer_message ?>
  </div>
  <?php endif; ?>
  
  
  <?php print $closure ?>

</body>

</html>
