<div id="<?php print $comment_id ?>" class="comment <?php print $classes ?>">
  
  <?php if ($new) : ?>
  <span class="new"><?php print $new ?></span>
  <?php endif; ?>
  
  <h3 class="title">
    <?php print $title ?>
  </h3>
  
  <div class="meta">
    <?php print $picture // div.picture img ?>
    <span class="submitted"><?php print $submitted ?></span>
  </div>
  
  <?php print $content // Comment content ?>
  
  <?php print $links // ul.links.comment-links ?>

</div>
